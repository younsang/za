$(document).ready(function(){
    if($('.range-rate').length){
        rangeSliderCustom();
    }
});

function rangeSliderCustom(){
    // $(".range-rate.percentage").ionRangeSlider({
    //     grid: true,
    //     from: 3,
    //     keyboard: true,
    //     values: [
    //         "2%", "98%"
    //     ],
    //     hide_min_max: false,
    //     hide_from_to: false,
    //     // min: 0,
    //     // max: 5000,
    //     // from: 1000,
    //     // to: 4000,
    //
    //     step: 1,
    //     // prefix: "$",
    //     // type: 'double', // 양쪽조절 및 한쪽조절
    //
    // });




    $("#range01").ionRangeSlider({
        grid: true,
        from: 0,
        keyboard: true,
        values: [6, 12, 18, 24, 30, 36],
        hide_min_max: true,
        hide_from_to: true,
        disabledClass: 'rangeslider--disabled',
        // min: 0,
        // max: 5000,
        // from: 1000,
        // to: 4000,

        step: 1,
        // prefix: "$",
        // type: 'double', // 양쪽조절 및 한쪽조절
    });

    $("#range02").ionRangeSlider({
        grid: true,
        from: 8,
        keyboard: true,
        values: [-10, -5, 0, 5, 10, 15, 20, 25, 30, 35, 40],
        hide_min_max: true,
        hide_from_to: true,
        // min: 0,
        // max: 5000,
        // from: 1000,
        // to: 4000,

        step: 1,
        // prefix: "$",
        // type: 'double', // 양쪽조절 및 한쪽조절
    });


}
