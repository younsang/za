$(document).ready(function(){
    userAgentCheck();
    layoutSystem();
    tabUi();
    inputUi();
    modalUi();
    downSlide();
    popupUi();
    tblUi();
    sliderUi();

    $('.ico-help').each(function(){
        $(this).on('click',function(){
            $(this).addClass('on');
            $('.coach-mark').fadeIn(250);
            $('html').css('overflow','hidden');
            if($(this).hasClass('on')){
                $('html').click(function(e) {
                    if(!$(e.target).hasClass("on")) {
                        $('.coach-mark').fadeOut();
                        $('html').css('overflow','auto');
                    }
                });
            }
        });
    });

    if($('.datepicker').length){
        libDatepicker();
    }
    if($('.header-sub h2 a').length){
        $('.header-sub h2 a').text(appTitle);
    }
    if($('.header-sub .txt-link').length){
        $('.header-sub .txt-link').text(appLink);
    }

});

$(window).on('scroll touchmove', function() {
    invScrollUi();
});



function userAgentCheck(){
    var ua = window.navigator.userAgent;
    var other = 999;
    var msie = ua.indexOf('MSIE ');

    if(ua.indexOf('Mobile') != -1){
        $('html').addClass('mobile');
    }

    if(ua.toLowerCase().indexOf('safari') != -1){
        if(ua.toLowerCase().indexOf('chrome') != -1){
            $('html').addClass('chrome');
        } else {
            $('html').addClass('safari');
        }
    } else if(ua.toLowerCase().indexOf('firefox') != -1){
        $('html').addClass('firefox');
    } else if(ua.toLowerCase().indexOf('msie 9.0') != -1){
        $('html').addClass('ie ie9');
    } else if(ua.toLowerCase().indexOf('msie 10.0') != -1){
        $('html').addClass('ie ie10');
    } else if(ua.toLowerCase().indexOf('rv:11.0') != -1){
        $('html').addClass('ie ie11');
    }

    if( ua.toLowerCase().indexOf('os x') != -1 ){
        $('html').addClass('ios');
    } else if( ua.toLowerCase().indexOf('Android') != -1 ){
        $('html').addClass('android');
    }
}

function layoutSystem(){

    if($('header').length === 0){
        $('#container.mypage-container').css({'padding-top':'0'});
    }

    if($('.header-sub').length){
        $('#container').css({'padding-top':'53px'});
    }

    // tab bar 만 존재할 때.
    if($('.btm-bar').length){
        $('#container').css({'padding-bottom':'80px'});
        $('.fix-inner.fix').css({'bottom' : '80px'});
    }
    // scroll fix btn 만 존재할 때.
    if($('.scroll-fix').length){
        $('#container').css({'padding-bottom' : '80px'});
    }
    // tab bar 와 scroll fix btn 가 존재할 때.
    if($('.scroll-fix').length && $('.btm-bar').length){
        $('.scroll-fix').css({'bottom' : '60px'});
        $('#container').css({'padding-bottom' : '160px'});
    }
    // 시뮬레이션 박스
    if($('.fix-dataBox').length){
        simulationUi();
        $('#container').css({'padding-bottom' : '0'});
    }
}

function tabUi(){

    var tabTit = $('.txt-list-tab'),
        tabBtn = tabTit.find('li');

    var tabCnt = $('.tab-content'),
        tabIdx = tabCnt.index();

    // load style settings
    tabCnt.not(':eq('+tabIdx+')').hide();
    tabTit.each(function(){
        var defaultTit = $(this).children('li').eq(0);
        defaultTit.addClass('on');
    });
    $('.tab-component').each(function () {
        var defaultCnt = $(this).children('.tab-content').eq(0);
        defaultCnt.addClass('on').show();
    });


    tabBtn.on('click', function(e){
        if($(this).attr('rel')){
            e.preventDefault();

            var $this = $(this),
                thisRel = $this.attr('rel');
                thisClass = $('.'+ thisRel);
                thisText = $this.text();
                target = thisClass.parent('.tab-component').attr('id');

            // content connect
            $('#' + target +  '>.tab-content').hide().removeClass('on');
            $('#' + target + ' .' + thisRel).show().addClass('on');

            // title styling and attr status
            $this.addClass('on').siblings().removeClass('on');
            thisClass.addClass('on').siblings().removeClass('on');
            $this.find('a').attr('title', thisText + 'tab active').parent().siblings().find('a').attr('title','');
        }
    });


    $('.horizen-ul').find('li').on('click',function(){
        $( 'html, body' ).animate( { scrollTop : 0 }, 400 );
	       return false;
    });
}

function inputUi(){
    $('select').on('change', function (e) {
        $(this).addClass('active');
       var optionSelected = $("option:selected", this);
       var valueSelected = this.value;
       if(valueSelected === 'etc'){
            $('.etc-input').addClass('on');
       } else {
           $('.etc-input').removeClass('on');
       }


   });

    $('.switch').each(function(){
        $(this).on('click', function(){
            if ($(this).hasClass('on')) {
                $(this).removeClass('on');
                $(this).find('input').attr('checked', false);
                return false;
            }else {
                $(this).addClass('on');
                $(this).find('input').attr('checked', true);
                return false;
            }
        });
    });

    $('.help').each(function(){
        $(this).on('click',function(e){
            e.preventDefault();
            $(this).toggleClass('on');
            if($(this).hasClass('on')){
                $('html').click(function(e) {
                    if(!$(e.target).hasClass("on")) {
                       $('.help').removeClass('on');
                    }
                });

            }
        });
    });
}

function modalUi(){
    $('.modal').each(function(){
        var layerResize = $(window).height();
        var layerHeight = $(this).outerHeight();
        var layerWidth = $(this).outerWidth();
        $(this).css({
            marginTop : -layerHeight/2
        });
    });
}


function libDatepicker(){
    $('.datepicker').datepicker({
        dateFormat: 'yy-mm-dd',
        showMonthAfterYear: true, //  년 뒤에 월 표시
        changeMonth: false,
        changeYear: false,
        //minDate:0, // today(0) 기준 이전 날짜 선택 불가
        changeMonth : true,
        changeYear : true,
        altField: ".select-date",
        altFormat: "yy-mm-dd(DD)",
        dayNames : ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', ],
        dayNamesMin : ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        dayNamesShort : ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
        dayNames: ['일','월','화','수','목','금','토'], // 요일 텍스트 설정
        dayNamesShort: ['일','월','화','수','목','금','토'], // 요일 텍스트 축약 설정&nbsp;
         firstDay: 1,//  0: 일요일 부터 시작, 1:월요일 부터 시작
        // yearSuffix: '<span>년</span>',
        onSelect: function() {
        }
    });
}

function downSlide(){
    $('.down-slide li a').on("click", function(){
        $(this).parent('li').toggleClass('on').find('.down-slide-inner').slideToggle();

    });
}

function popupUi(){
    // create sizing
    var layerHeight = $('.popup-cont').outerHeight();
    var layerWidth = $('.popup-cont').outerWidth();
    $('.popup-cont').css({
        marginTop : -layerHeight/2
    });
}

function tblUi(){
    $('.tbl-arrow').on("click", function(e){
        e.preventDefault();
        $(this).toggleClass('on').parents('tr').next('.hide-cont').toggleClass('on');
    });
}

function sliderUi(){
    $('.main-slider .slider-container').slick({
        infinite: false,
        slidesToShow: 1,
        arrows: false,
        slidesToScroll: 1,
        mobileFirst: false,
        dots: true
    });
    $('.main-slider .slider-container').find('.slick-dots').wrap('<div class="slick-dots-box"></div>');
}

function invScrollUi(){
    var scrollTop = $(window).scrollTop();
    var headerH = $('header').outerHeight();
    $('.scroll-ui-bar').each(function(){
        if(scrollTop > 0){
            $(this).addClass('fix').css('top',headerH);
            // var changeUIH = $(this).outerHeight();
            $(this).parents('#wrap').css('padding-top','151px');
        } else {
            $(this).removeClass('fix').css('top','auto');
            $(this).parents('#wrap').css('padding-top','0');
        }
    });
}


function simulationUi(){
    var divOffset = $('#simulResult').offset();
    $(window).on('scroll touchmove', function(){
        if( $(document).scrollTop() > divOffset.top -500){
            $('.fix-dataBox').addClass('remove');
        } else {
            $('.fix-dataBox').removeClass('remove');
        }
    });


    $('#simulResult .tit-txt .arrow').on("click", function(e){
        e.preventDefault();
        $(this).parents('#simulResult').toggleClass('tbl-remove');
    });

    $('.rounded-btn li').on('click', function(e){
        e.preventDefault();
        $(this).addClass('on').siblings().removeClass('on');
    });
}
